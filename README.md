# pmg_xsec_db

Keeping track of the PMG cross section database!

## How it do

Use the file [cron/pmg_xsec_db_update.sh](cron/pmg_xsec_db_update.sh) in a
cron job to copy the centrally managed cross section files on *cvmfs/*:

```bash
/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt
/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt_previous
```

and push them to this repository. The CI will produce an artifact that is the diff
between these files that can then be inspected.

Future updates can be a notification being issued following some heuristic on 
"something going wrong" with the xsec files. For now, this repository will just
serve as a running timeline of the changes to the xsec files.

Currently, the cron job I use is to update the xsec files every 12 hours.
