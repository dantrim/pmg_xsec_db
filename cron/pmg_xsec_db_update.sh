#!/bin/bash
cd /home/dantrim/cron/
startdir=${PWD}
echo "INFO    `date`"
echo "INFO    Updating PMG xsec database"
gitdir="/data/uclhc/uci/user/dantrim/pmg_xsec_db/"
if [[ ! -d ${gitdir} ]]; then
    echo "ERROR    Expected location (=${gitdir}) not found, cannot update PMG xsec database"
else
    echo "INFO    Moving to ${gitdir}"
    cd $gitdir
    git remote -v
    file0="PMGxsecDB_mc16.txt"
    file1="PMGxsecDB_mc16.txt_previous"
    fullfile0="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt"
    fullfile1="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt_previous"
    if [[ ! -f ${fullfile0} ]]; then
        echo "WARNING    Expected file (=${fullfile0}) not found, cannot update this file"
    else
        echo "INFO    Copying ${fullfile0}"
        cp ${fullfile0} ./data/
        git add ./data/${file0}
    fi
    if [[ ! -f ${fullfile1} ]]; then
        echo "WARNING    Expected file (=${fullfile1}) not found, cannot update this file"
    else
        echo "INFO    Copying ${fullfile1}"
        cp ${fullfile1} ./data/
        git add ./data/${file1}
    fi
    echo "INFO    Commiting changes..."
    datetime=$(date "+%F-%T")
    #echo "datetime = ${datetime}"
    commitmsg="xsec update ${datetime}"
    #echo "commitmsg = ${commitmsg}"
    git commit -m "${commitmsg}"
    echo "INFO    Pushing changes..."
    git push origin master
    echo "INFO    Done!"
fi
cd $startdir
