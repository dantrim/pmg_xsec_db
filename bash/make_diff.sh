#!/usr/bin/env bash
current=$(git log --pretty=%H |head -n 1)
previous=$(git log --pretty=%H |head -n 2 |tail -n 1)

echo "current = ${current}"
echo "previous = ${previous}"
git diff ${previous} ${current} 2>&1 |tee xsec_diff.diff
